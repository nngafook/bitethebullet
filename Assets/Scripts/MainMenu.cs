﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UdpKit;
using Bolt;

public class MainMenu : GlobalEventListener {

    private enum State {
        FirstConnect,
        ConnectAsClient,
        SearchingForServer,
        CheckingForServerAsHost,
        CheckingForServerAsClient,
        ConnectAsHost,
        QuickHost,
        QuickJoin,
        Waiting
    }

    private State state = State.Waiting;

    private int sessionRefreshCounter = 0;

    private bool serverExists = false;

    //private List<string> randomNames = new List<string>();

    private UdpSession sessionToLoad = null;

    public InputField nameInputField;
    public ColorButtonMenu buttonMenu;
    public Text errorMessageLabel;

    public Text loadingMessageLabel;

    [Space(10)]
    public Button joinButton;
    public Button hostButton;

    [Space(10)]
    public CanvasGroup titleCG;
    public CanvasGroup networkMenuCG;
    public CanvasGroup playerMenuCG;
    public CanvasGroup loadingCG;

    void Awake() {
        titleCG.SetAlphaAndBools(true);
        playerMenuCG.SetAlphaAndBools(false);
        networkMenuCG.SetAlphaAndBools(false);
        loadingCG.SetAlphaAndBools(false);
    }

    #region NETWORKING
    private void OnCheckForSessionComplete() {
        switch (state) {
            case State.FirstConnect:
                ShutdownBolt();
                break;
            case State.ConnectAsClient:
                break;
            case State.SearchingForServer:
                break;
            case State.CheckingForServerAsHost:
                ShutdownBolt();
                break;
            case State.CheckingForServerAsClient:
            case State.QuickJoin:
                if (serverExists && BoltNetwork.isClient) {
                    state = State.ConnectAsClient;
                    loadingMessageLabel.text = "CONNECTING TO SERVER";

                    loadingCG.SetAlphaAndBools(false);
                    playerMenuCG.SetAlphaAndBools(true);
                }
                else if (!serverExists) {
                    ShutdownBolt();
                }
                break;
            case State.ConnectAsHost:
                break;
            case State.Waiting:
                break;
            default:
                break;
        }
    }

    #region NETWORK_CALLBACKS
    public override void ZeusConnected(UdpEndPoint endpoint) {
        Debug.Log("MainMenu Zeus Connected");
        Bolt.Zeus.RequestSessionList();

        switch (state) {
            case State.ConnectAsHost:
                if (BoltNetwork.isServer) {
                    loadingMessageLabel.text = "LOADING";
                    BoltNetwork.SetHostInfo("- TS Test Server -", null);
                }
                break;
            case State.QuickHost:
                if (BoltNetwork.isServer) {
                    loadingMessageLabel.text = "LOADING";
                    BoltNetwork.SetHostInfo("- TS Test Server -", null);
                    BoltNetwork.LoadScene("TestPlace");
                }
                break;
            case State.Waiting:
                loadingCG.SetAlphaAndBools(false);
                networkMenuCG.SetAlphaAndBools(true);
                break;
        }
    }

    public override void BoltStartDone() {
        Debug.Log("- Bolt Start Done -");
    }


    public void ShutdownBolt() {
        BoltLauncher.Shutdown();
    }

    public override void SessionListUpdated(Map<System.Guid, UdpSession> sessionList) {
        BoltLog.Info("Session list updated.");
        sessionRefreshCounter++;
        if (sessionRefreshCounter == 2) {
            CheckForSession();
            sessionRefreshCounter = 0;
        }
    }

    public override void BoltShutdownBegin(AddCallback registerDoneCallback) {
        registerDoneCallback(OnBoltShutdownComplete);
        base.BoltShutdownBegin(registerDoneCallback);
    }

    public override void SessionConnectFailed(UdpSession session, IProtocolToken token) {
        BoltLog.Info("Failed to connect to session.");
    }

    public override void ZeusConnectFailed(UdpEndPoint endpoint) {
        BoltLog.Info("Failed to connect to Zeus.");
    }

    public override void ZeusDisconnected(UdpEndPoint endpoint) {
        BoltLog.Info("Disconnected from Zeus.");
    }

    public override void ZeusNatProbeResult(NatFeatures features) {
        BoltLog.Info("Receieved results of Zeus NAT Probe.");
    }
    
    #endregion NETWORK_CALLBACKS


    public void OnBoltShutdownComplete() {
        Debug.Log("Bolt Shutdown Complete");
        switch (state) {
            case State.FirstConnect:
                state = State.Waiting;
                hostButton.interactable = true;
                loadingCG.SetAlphaAndBools(false);
                networkMenuCG.SetAlphaAndBools(true);
                break;
            case State.CheckingForServerAsHost:
                if (serverExists) {
                    state = State.Waiting;
                    loadingCG.SetAlphaAndBools(false);
                    networkMenuCG.SetAlphaAndBools(true);
                    errorMessageLabel.text = "Server Already Running";
                }
                else {
                    if (!BoltNetwork.isClient) {
                        state = State.ConnectAsHost;
                        loadingMessageLabel.text = "CREATING SERVER";
                        BoltLauncher.StartServer(UdpKit.UdpEndPoint.Parse("0.0.0.0:27000"));
                        loadingCG.SetAlphaAndBools(false);
                        playerMenuCG.SetAlphaAndBools(true);
                    }
                    else {
                        Debug.Log("Trying to start server, but isClient is true");
                    }
                }
                break;
            case State.CheckingForServerAsClient:
                if (serverExists && BoltNetwork.isClient) {
                    state = State.ConnectAsClient;
                    loadingMessageLabel.text = "CONNECTING TO SERVER";
                    Debug.Log("Connecting to: " + sessionToLoad.HostName);
                    //BoltNetwork.Connect(sessionToLoad);

                    loadingCG.SetAlphaAndBools(false);
                    playerMenuCG.SetAlphaAndBools(true);
                }
                else if (!serverExists) {
                    loadingCG.SetAlphaAndBools(false);
                    networkMenuCG.SetAlphaAndBools(true);
                    errorMessageLabel.text = "No Server Found";
                }
                break;
            case State.SearchingForServer:
                
                break;
            case State.ConnectAsHost:
                
                break;
            case State.Waiting:
                break;
            default:
                break;
        }
    }


    #endregion NETWORKING
    public void QuickHost() {
        state = State.QuickHost;
        titleCG.SetAlphaAndBools(false);
        loadingCG.SetAlphaAndBools(true);

        BoltLauncher.StartServer(UdpKit.UdpEndPoint.Parse("0.0.0.0:27000"));
    }

    public void QuickJoin() {
        state = State.QuickJoin;
        titleCG.SetAlphaAndBools(false);
        loadingCG.SetAlphaAndBools(true);

        BoltLauncher.StartClient();
    }

    public void OnTitleStartPressed() {
        state = State.FirstConnect;
        BoltLauncher.StartClient();
        titleCG.SetAlphaAndBools(false);
        loadingCG.SetAlphaAndBools(true);
        loadingMessageLabel.text = "SPEAKING TO THE INTERNET";
    }

    public void OnRandomPressed() {
        string name = Utility.RandomName();
        nameInputField.text = name;
        buttonMenu.RandomColor();
    }

    public void OnConfirmPressed() {
        PlayerStats.Instance.name = nameInputField.text;
        PlayerStats.Instance.color = buttonMenu.SelectedColor;

        playerMenuCG.SetAlphaAndBools(false);
        loadingCG.SetAlphaAndBools(true);

        if (state == State.ConnectAsHost) {
            BoltNetwork.LoadScene("TestPlace");
        }
        else if (state == State.ConnectAsClient) {
            BoltNetwork.Connect(sessionToLoad);
        }
    }

    public void OnHostPressed() {
        playerMenuCG.SetAlphaAndBools(false);
        networkMenuCG.SetAlphaAndBools(false);
        loadingCG.SetAlphaAndBools(true);
        loadingMessageLabel.text = "SEARCHING FOR EXISTING SERVER";

        state = State.CheckingForServerAsHost;
        BoltLauncher.StartClient();
    }

    public void OnJoinPressed() {
        networkMenuCG.SetAlphaAndBools(false);
        loadingCG.SetAlphaAndBools(true);
        loadingMessageLabel.text = "SEARCHING FOR SERVER";

        state = State.CheckingForServerAsClient;
        BoltLauncher.StartClient();
    }

    public void CheckForSession() {
        if (!serverExists && sessionToLoad == null) {
            foreach (var session in BoltNetwork.SessionList) {
                if (session.Value.HostName.IndexOf("- TS") == 0) {
                    sessionToLoad = session.Value;
                    joinButton.interactable = true;
                    serverExists = true;
                    Debug.Log("server exists");
                }
            }
            if (serverExists) {
                hostButton.interactable = false;
            }
        }
        OnCheckForSessionComplete();
    }

    void Update() {
        
    }

}
