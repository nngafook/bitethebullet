﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIController : MonoBehaviour {

    private static UIController instance = null;
    public static UIController Instance { get { return instance; } }

    public bool ChatInputFocused { get { return chatInputField.isFocused; } }

    public List<PlayerUI> playerUIList;

    public InputField chatInputField;
    public ChatLog chatLog;

    [Header("Canvas Groups")]
    public CanvasGroup sendButtonCG;

    void Awake() {
        instance = this;

    }

	// Use this for initialization
	void Start () {
        SetAllUIsActive(false);

        chatInputField.interactable = false;
        sendButtonCG.SetAlphaAndBools(false);
        chatLog.SetVisible(false);

        chatInputField.onValueChanged.AddListener(OnChatInputChanged);
	}

    private void OnChatInputChanged(string value) {
        if (chatInputField.text != chatInputField.text.ToUpper()) {
            chatInputField.text = chatInputField.text.ToUpper();
        }
    }

    private void OnEnterPressed() {
        if (chatInputField.interactable) {
            // Send message, and deactivate Input
            if (!string.IsNullOrEmpty(chatInputField.text)) {
                SendChatMessage();
                chatInputField.text = "";
            }
            sendButtonCG.SetAlphaAndBools(false);
            chatLog.SetVisible(false);
            chatInputField.interactable = false;
            chatInputField.DeactivateInputField();
        }
        else {
            // Activate Input
            sendButtonCG.SetAlphaAndBools(true);
            chatLog.SetVisible(true);
            chatInputField.interactable = true;
            chatInputField.ActivateInputField();
            chatInputField.Select();
        }
    }

    private void SetAllUIsActive(bool value) {
        for (int i = 0; i < playerUIList.Count; i++) {
            playerUIList[i].SetActive(value);
        }
    }

    /// <summary>
    /// Dispatches an event that when received, the player sends their name, color etc for the UIController to set the
    /// corresponding PlayerUIs.
    /// </summary>
    public void GetClientsInfo() {
        NetworkManager.Instance.DispatchGetPlayerInfoEvent();
    }

    /// <summary>
    /// Returns the player UI owned by the passed in ID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private PlayerUI GetPlayerUIByID(int id) {
        return playerUIList[id - 1];
    }

    /// <summary>
    /// Sets the UI of the id passed in to be visible. This should really
    /// only be called once, when the player connects
    /// </summary>
    /// <param name="playerNumber"></param>
    /// <returns></returns>
    public PlayerUI SetPlayerUIVisible(int playerNumber) {
        PlayerUI rVal = GetPlayerUIByID(playerNumber);
        
        if (!rVal.Active) {
            rVal.SetActive(true);
        }

        return rVal;
    }

    /// <summary>
    /// Should only be used when a player joins the game
    /// </summary>
    /// <param name="e"></param>
    public void SetPlayerUI(PlayerStatsEvent e) {
        Debug.Log("*SetPlayerUI* - My ID: " + PlayerStats.Instance.playerID + ", Passed ID: " + e.ID);
        PlayerUI ui = SetPlayerUIVisible(e.ID);
        ui.Claim(e);
    }

    public void UpdateClientStamina(int id, float current, float max) {
        PlayerUI ui = GetPlayerUIByID(id);
        ui.UpdateClientStamina(current, max);
    }

    /// <summary>
    /// Used to update client UIs
    /// </summary>
    /// <param name="e"></param>
    public void UpdateClientUI(PlayerStatsEvent e) {
        if (e.ID != PlayerStats.Instance.playerID) {
            PlayerUI ui = GetPlayerUIByID(e.ID);
            ui.UpdateClientUI(e);
        }
    }

    public void SendChatMessage() {
        NetworkManager.Instance.DispatchChatMessage(PlayerStats.Instance.name, PlayerStats.Instance.color, chatInputField.text);
    }

    public void AddChatMessage(string message) {
        chatLog.AddMessage("SERVER: " + message);
    }

    public void AddChatMessage(string playerName, Color playerColor, string message) {
        chatLog.AddMessage(playerName, playerColor, message);
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.KeypadEnter) || Input.GetKeyUp(KeyCode.Return)) {
            OnEnterPressed();
        }
	}
}
