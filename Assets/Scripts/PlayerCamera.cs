﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {

    public bool isSceneCamera = false;

    public Vector3 offset;
    
    [HideInInspector]
    public Transform targetPlayer;

    void Awake() {
        if (isSceneCamera) {
            DestroyImmediate(this.gameObject);
        }
    }

	// Use this for initialization
	void Start () {
        
	}

    public void SetTarget(Transform t) {
        targetPlayer = t;
    }

	// Update is called once per frame
    void Update() {
        
    }

    void FixedUpdate() {
        if (targetPlayer != null) {
            transform.position = Vector3.Lerp(transform.position, targetPlayer.position + offset, 2 * Time.deltaTime);
        }
	}

}
