﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class ChatLog : MonoBehaviour {

    private float visibleDelay = 5; // In seconds
    private CanvasGroup mainCG;

    public GameObject chatLogMessageObject;
    public InputField chatInputField;
    public Transform listParent;

    void Awake() {
        mainCG = this.GetComponent<CanvasGroup>();
    }

	// Use this for initialization
	void Start () {
	
	}

    private IEnumerator VisibleDelay() {
        if (!chatInputField.isFocused) {
            yield return new WaitForSeconds(visibleDelay - 1);
            mainCG.DOFade(0, 1).OnComplete(OnFadeComplete);
        }
    }

    private void OnFadeComplete() {
        SetVisible(false);
    }

    public void SetVisible(bool value) {
        mainCG.SetAlphaAndBools(value);
        if (value) {
            mainCG.DOKill();
            StopCoroutine("VisibleDelay");
        }
    }

    public void AddMessage(string message) {
        ChatLogMessage chatLogMessage = Instantiate(chatLogMessageObject, Vector3.zero, Quaternion.identity).GetComponent<ChatLogMessage>();
        chatLogMessage.transform.SetParent(listParent, false);
        chatLogMessage.SetText(message);
        
        SetVisible(true);
        StartCoroutine("VisibleDelay");
    }

    public void AddMessage(string playerName, Color playerColor, string message) {
        ChatLogMessage chatLogMessage = Instantiate(chatLogMessageObject, Vector3.zero, Quaternion.identity).GetComponent<ChatLogMessage>();
        chatLogMessage.transform.SetParent(listParent, false);
        chatLogMessage.SetText(playerName, playerColor, message);

        SetVisible(true);
        StartCoroutine("VisibleDelay");
    }

	// Update is called once per frame
	void Update () {
	
	}
}
