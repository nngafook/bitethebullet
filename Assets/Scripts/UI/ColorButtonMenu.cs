﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorButtonMenu : MonoBehaviour {

    private int selectedIndex = -1;
    private Color selectedColor;

    public List<ColorSelectButton> buttons;

    public Color SelectedColor { get { return selectedColor; } }

    void Awake() {
        SetButtonIndexes();
    }

	// Use this for initialization
	void Start () {
	    
	}

    private void SetButtonIndexes() {
        for (int i = 0; i < buttons.Count; i++) {
            buttons[i].SetIndex(i);
        }
    }

    public void SelectIndex(int index, Color color) {
        if (selectedIndex != -1) {
            buttons[selectedIndex].Deselect();
        }
        selectedIndex = index;
        selectedColor = color;
        buttons[selectedIndex].Select();
    }

    public void RandomColor() {
        int randomIndex = Random.Range(0, buttons.Count);
        SelectIndex(randomIndex, buttons[randomIndex].Color);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
