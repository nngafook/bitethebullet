﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Text;

public class ChatLogMessage : MonoBehaviour {

    private Text messageLabel;

    void Awake() {
        messageLabel = this.GetComponent<Text>();
    }

	// Use this for initialization
	void Start () {
	
	}

    public void SetText(string message) {
        DateTime now = DateTime.Now;
        //string timestamp = "[" + now.ToString("h") + ":" + now.ToString("mm") + now.ToString("tt") + "] ";
        string timestamp = "[" + now.ToString("h:mm") + now.ToString("tt") + "] ";

        StringBuilder sb = new StringBuilder();
        sb.Append(timestamp);
        sb.Append(message);

        messageLabel.text = sb.ToString();
    }

    public void SetText(string playerName, Color playerColor, string message) {
        DateTime now = DateTime.Now;
        //string timestamp = "[" + now.ToString("h") + ":" + now.ToString("mm") + now.ToString("tt") + "] ";
        string timestamp = "[" + now.ToString("h:mm") + now.ToString("tt") + "] ";

        StringBuilder sb = new StringBuilder();
        sb.Append(timestamp);
        sb.Append(playerName.Colored(playerColor));
        sb.Append(": " + message);

        messageLabel.text = sb.ToString();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
