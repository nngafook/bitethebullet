﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NetworkLoadingIcon : MonoBehaviour {

    private int direction = 1;
    private int currentIndex = 0;

    public List<RectTransform> icons;

	// Use this for initialization
	void Start () {
	    Next();
	}

    private void Next() {
        icons[currentIndex].DOScale(1.6f, 0.15f).SetLoops(2, LoopType.Yoyo).OnComplete(Next);
        icons[currentIndex].GetComponent<Image>().DOColor(Color.green, 0.15f).SetLoops(2, LoopType.Yoyo);

        currentIndex += direction;
        if (currentIndex == icons.Count) {
            direction *= -1;
            currentIndex = icons.Count - 2;
        }
        else if (currentIndex < 0) {
            direction *= -1;
            currentIndex = 1;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
