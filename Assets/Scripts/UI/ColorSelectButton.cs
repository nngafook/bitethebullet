﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class ColorSelectButton : MonoBehaviour {

    private float scaleSpeed = 0.1f;
    private int listIndex = -1;
    private Color color;
    public Color Color { get { return color; } }

    public ColorButtonMenu menu;

    void Awake() {
        color = this.GetComponent<Button>().colors.normalColor;
        this.GetComponent<Button>().onClick.AddListener(OnPressed);
    }

	// Use this for initialization
	void Start () {
	
	}

    public void SetIndex(int index) {
        listIndex = index;
    }

    public void Select() {
        this.RT().DOScale(1.6f, scaleSpeed);
    }

    public void Deselect() {
        this.RT().DOScale(1f, scaleSpeed);
    }

    public void OnPressed() {
        menu.SelectIndex(listIndex, color);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
