﻿using UnityEngine;
using System.Collections;
using PathologicalGames;

public class Projectile : MonoBehaviour {

    private float speed = 2;

    public bool playerOwned = true;
    [Tooltip("In Seconds")]
    public float lifetime = 2;

    void Awake() {
        if (this.playerOwned) {
            this.gameObject.tag = "PlayerProjectile";
            this.gameObject.layer = LayerMask.NameToLayer("PlayerProjectile");
        }
        else {
            this.gameObject.tag = "EnemyProjectile";
            this.gameObject.layer = LayerMask.NameToLayer("EnemyProjectile");
        }
    }

	// Use this for initialization
	void Start () {
	
	}

    //#region NETWORKING
    //public override void Attached() {
    //    //state.SetTransforms(state.Transform, transform);
    //}
    //#endregion

    void OnSpawned() {
        lifetime = 2;
    }

    void OnDespawned() {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }

    public void Fire(float s) {
        speed = s;
        GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.VelocityChange);
    }

    public void OnCollisionEnter(Collision collision) {
        ContactPoint contact = collision.contacts[0];
        
        PoolManager.Pools["Projectiles"].Despawn(this.transform);
        NetworkManager.Instance.SpawnFromPool("VFX", "SmallExplosion", contact.point, Quaternion.identity);
    }

    

    public void OnTriggerEnter(Collider other) {
        
    }

	// Update is called once per frame
	void Update () {
        lifetime -= Time.deltaTime;
        if (lifetime <= 0) {
            PoolManager.Pools["Projectiles"].Despawn(this.transform);
        }
	}
}
