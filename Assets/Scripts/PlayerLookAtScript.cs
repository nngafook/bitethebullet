﻿using UnityEngine;
using System.Collections;

public class PlayerLookAtScript : MonoBehaviour {

    private Vector3 lookTarget;
    public float turnSpeed = 20.0f;
    public Camera playerCamera;

    //private float speed = 10.0f;

	// Use this for initialization
	void Start () {
	}

    private void SmoothRotate() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit)) {
            lookTarget = hit.point;
        }

        // rotate towards target
        var lookDelta = (hit.point - transform.position);
        var targetRot = Quaternion.LookRotation(lookDelta);
        var rotSpeed = turnSpeed * Time.deltaTime;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRot, rotSpeed);
    }

    private void SnapRotate() {
        //if (networkView.isMine) {
            Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                lookTarget = hit.point;
            }

            transform.LookAt(new Vector3(lookTarget.x, transform.position.y, lookTarget.z));
        //}
    }

    private void OtherRotate() {
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist)) {
            // Get the point along the ray that hits the calculated distance.
            var targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            var targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            // Smoothly rotate towards the target point.
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
        }
    }

	// Update is called once per frame
	void Update () {
        OtherRotate();
	}

}
