﻿using UnityEngine;
using System.Collections;
using Rewired;
using Bolt;
using PathologicalGames;
using System.Collections.Generic;

public class Player : Bolt.EntityEventListener<IPlayerState> {
    
    private Rewired.Player rewiredPlayer; // The Rewired Player
    private Rigidbody rBody;

    private PlayerUI playerUI;

    private Vector3 moveVector;

    private Vector3 lastShootVector;
    private Vector3 shootVector;
    private Quaternion turnToRotation;

    private bool canShoot = true;
    private bool isTurning = false;

    #region INPUT_BOOLS
    // Right Stick is being pushed
    private bool fire = false;
    private bool dash = false;
    #endregion INPUT_BOOLS

    #region DASHING
    // Counter used to count frames while dashing
    private float dashCounter = 0;
    // Number of frames a dash lasts
    [SerializeField]
    private float dashLength = 18;
    // Number of frames added to the dashLength to pause after a dash
    private float dashPauseLength = 5;
    // True while dashing
    private bool isDashing = false;
    // After a dash, pause the player movement for a few frames. TRUE while paused
    private bool dashPaused = false;
    #endregion DASHING

    public int rewiredPlayerID = 0; // The Rewired player id of this character
    [SerializeField]
    private float dashSpeed = 20;
    [SerializeField]
    private float moveSpeed = 15f;
    [SerializeField]
    private float bulletSpeed = 50f;
    [Tooltip("Shots Per Second")]
    [SerializeField]
    public float rateOfFire = 5f;

    public Transform gunBarrel;
    public Transform feet;

    public List<Renderer> modelRenderers;

    [Space(10)]
    public BoltEntity bulletPrefab;

    private VFXNetworkPrefabPool vfxPool;

    void Awake() {
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerID);

        this.gameObject.tag = "Player";
        this.gameObject.layer = LayerMask.NameToLayer("Player");

        rBody = GetComponent<Rigidbody>();
    }

    #region NETWORKING
    public override void Attached() {
        state.SetTransforms(state.Transform, transform);
        state.Stamina = PlayerStats.Instance.currentStamina;
        state.AddCallback("Stamina", OnStaminaChanged);

        Debug.Log("-*-*- Attached -*-*-*");

        if (entity.isOwner) {
            Object o = Resources.Load("Prefabs/PlayerCamera");
            PlayerCamera playerCamera = (Instantiate(o) as GameObject).GetComponent<PlayerCamera>();
            playerCamera.SetTarget(this.transform);

            if (BoltNetwork.isServer) {
                PlayerStats.Instance.playerID = 1;
            }
            else {
                BoltConnection connection = BoltNetwork.server;
                PlayerStats.Instance.playerID = (int)connection.ConnectionId;
            }
            state.PlayerID = PlayerStats.Instance.playerID;
            
            PlayerStats.Instance.networkID = (int)entity.networkId.PackedValue;
            
            UIController.Instance.AddChatMessage("My Network ID is: " + (int)entity.networkId.PackedValue);
            //UIController.Instance.AddChatMessage("PlayerID Set as: " + PlayerStats.Instance.playerID);
            NetworkManager.Instance.DispatchChatMessage(PlayerStats.Instance.ColoredName, PlayerStats.Instance.color, "I HAVE ARRIVED!");

            NetworkManager.Instance.AddPlayer(PlayerStats.Instance.networkID, PlayerStats.Instance.playerID);

            // There's a check in SetPlayerUIVisible that does not set it if it's already active.
            // This should not affect this call, as this is the initial Set for the LOCAL player
            playerUI = UIController.Instance.SetPlayerUIVisible(PlayerStats.Instance.playerID);
            playerUI.SetInfo();

            UIController.Instance.GetClientsInfo();
            NetworkManager.Instance.DispatchRegisterIDEvent();
            for (int i = 0; i < modelRenderers.Count; i++) {
                modelRenderers[i].material.SetColor("_Tint", PlayerStats.Instance.color);
            }
        }

    }

    public override void OnEvent(ConnectionEvent evnt) {
        Debug.Log("-*-*- Player On Connection Event -*-*-*");
        base.OnEvent(evnt);
    }

    private void OnStaminaChanged() {
        UIController.Instance.UpdateClientStamina(state.PlayerID, state.Stamina, 100);
    }
    #endregion

    private IEnumerator ProcessShootability() {
        canShoot = false;
        yield return new WaitForSeconds(1 / rateOfFire);
        canShoot = true;
    }

    private IEnumerator RotateTo() {
        float i = 0f;
        float rate = 1f / 0.15f;
        Quaternion startRot = transform.rotation;
        isTurning = true;
        while (i < 1f) {
            i += Time.deltaTime * rate;
            transform.rotation = Quaternion.Lerp(startRot, turnToRotation, i);
            yield return null;
        }
        isTurning = false;
    }

    private void GetInput() {
        if (entity.isOwner && !UIController.Instance.ChatInputFocused) {
            // Get the input from the Rewired Player. All controllers that the Player owns will contribute, so it doesn't matter
            // whether the input is coming from a joystick, the keyboard, mouse, or a custom controller.

            moveVector.x = rewiredPlayer.GetAxisRaw("Move Horizontal"); // get input by name or action id
            moveVector.z = rewiredPlayer.GetAxisRaw("Move Vertical");

            shootVector.x = rewiredPlayer.GetAxisRaw("Shoot Horizontal");
            shootVector.z = rewiredPlayer.GetAxisRaw("Shoot Vertical");

            fire = (shootVector.x != 0 || shootVector.z != 0);

            dash = rewiredPlayer.GetButtonDown("Dash");
        }
        else if (UIController.Instance.ChatInputFocused) {
            moveVector = Vector3.zero;
            shootVector = Vector3.zero;
        }
    }

    private void ProcessInput() {
        if (entity.isOwner) {
            // Process dash movement
            if (isDashing) {
                dashCounter++;

                if (dashPaused && dashCounter >= dashLength + dashPauseLength) {
                    dashCounter = 0;
                    isDashing = false;
                    dashPaused = false;
                }
                else if (!dashPaused) {
                    if (dashCounter > 5 && rBody.velocity.magnitude <= 1) {
                        dashCounter = 0;
                        isDashing = false;
                    }

                    if (dashCounter >= dashLength) {
                        dashPaused = true;
                        rBody.velocity = Vector3.zero;
                    }
                }
            }

            // Process dash input
            if (dash && !isDashing) {
                if (PlayerStats.Instance.currentStamina >= 50) {
                    PlayerStats.Instance.currentStamina -= 50;
                    
                    playerUI.UpdateUI();
                    //NetworkManager.Instance.DispatchUpdatePlayerUI();
                    
                    isDashing = true;
                    NetworkManager.Instance.SpawnFromPool("VFX", "DashSmokePuff", transform.position, transform.rotation);
                    Vector3 dashDirection = (moveVector.x != 0 || moveVector.z != 0) ? moveVector : transform.forward;
                
                    transform.rotation = (fire) ? transform.rotation : Quaternion.LookRotation(dashDirection.normalized);
                    rBody.velocity = (moveVector.x != 0 || moveVector.z != 0) ? moveVector.normalized * moveSpeed : transform.forward * moveSpeed;
                    rBody.AddForce(dashDirection * dashSpeed, ForceMode.Impulse);
                }
            }

            // Process movement
            if (!isDashing) {
                float speed = fire ? moveSpeed * 0.5f : moveSpeed;
                rBody.velocity = moveVector.normalized * speed;
            }

            // Process Rotation
            if (isTurning && !fire && moveVector == Vector3.zero) {
                StopCoroutine("RotateTo");
                turnToRotation = Quaternion.identity;
                isTurning = false;
            }
            if (!isDashing && fire) {
                if (shootVector != Vector3.zero) {
                    Quaternion newRot = Quaternion.LookRotation(shootVector);
                    if (newRot != turnToRotation) {
                        turnToRotation = newRot;
                        StopCoroutine("RotateTo");
                        StartCoroutine("RotateTo");
                    }
                }
            }
            else if (!isDashing) {
                if (moveVector.normalized != Vector3.zero) {
                    Quaternion newRot = Quaternion.LookRotation(moveVector.normalized);
                    if (newRot != turnToRotation) {
                        turnToRotation = newRot;
                        StopCoroutine("RotateTo");
                        StartCoroutine("RotateTo");
                    }
                }
            }

            // Process fire
            if (!isDashing && fire && canShoot) {
                NetworkManager.Instance.SpawnFromPool("Projectiles", "Bullet", gunBarrel.position, transform.rotation, bulletSpeed);

                StartCoroutine(ProcessShootability());
            }
        }
    }

    void OnGUI() {
        //GUI.color = Color.black;
        //GUI.Label(new Rect(5, 5, 200, 40), rBody.velocity.magnitude.ToString());
        //GUI.color = Color.white;
    }

    public override void SimulateOwner() {
        base.SimulateOwner();
    }

    void Update() {
        GetInput();
        ProcessInput();
        state.Stamina = PlayerStats.Instance.currentStamina;
    }

}
