﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {

    private bool isActive = false;

    private CanvasGroup mainCG;
    //private NetworkPlayerStats networkPlayerStats;
    private int ownerID = -1;

    public Text nameLabel;
    public Image healthBar;
    public Image staminaBar;

    public bool Active { get { return isActive; } }

    void Awake() {
        mainCG = this.GetComponent<CanvasGroup>();
    }

	// Use this for initialization
	void Start () {
	
	}

    private void RefillStamina() {
        if (PlayerStats.Instance != null && ownerID == PlayerStats.Instance.playerID) {
            if (PlayerStats.Instance.currentStamina < PlayerStats.Instance.maxStamina) {
                PlayerStats.Instance.currentStamina += PlayerStats.Instance.staminaRechargeRate;
                if (PlayerStats.Instance.currentStamina > PlayerStats.Instance.maxStamina) {
                    PlayerStats.Instance.currentStamina = PlayerStats.Instance.maxStamina;
                }
                //UpdateUI();
            }
        }
        //else if (networkPlayerStats != null) {
        //    if (networkPlayerStats.currentStamina < networkPlayerStats.maxStamina) {
        //        networkPlayerStats.currentStamina += networkPlayerStats.staminaRechargeRate;
        //        if (networkPlayerStats.currentStamina > networkPlayerStats.maxStamina) {
        //            networkPlayerStats.currentStamina = networkPlayerStats.maxStamina;
        //        }
        //        UpdateUI();
        //    }
        //}
        //else {
        //    Debug.LogError("networkPlayerStats was NULL in RefillStamina() - " + this.gameObject.name + " || " + ownerID + " || " + PlayerStats.Instance.playerID);
        //}
    }

    public void SetActive(bool value) {
        isActive = value;
        mainCG.SetAlphaAndBools(isActive);
    }

    public void SetID(int id) {
        ownerID = id;
    }

    /// <summary>
    /// Used when a player joins the game and claims this UI.
    /// </summary>
    /// <param name="id"></param>
    public void Claim(PlayerStatsEvent e) {
        //networkPlayerStats = new NetworkPlayerStats();
        //networkPlayerStats.CopyStats(e);
        nameLabel.text = e.Name.Colored(e.Color) + " (" + e.ID + ")";
        SetID(e.ID);
        //UpdateClientUI(e);
    }

    public void UpdateClientStamina(float current, float max) {
        staminaBar.fillAmount = Mathf.Clamp(((current / max)), 0, max);
    }

    /// <summary>
    /// When a client sends their info for all other clients to update
    /// </summary>
    public void UpdateClientUI(PlayerStatsEvent e) {
        //if (e.ID != PlayerStats.Instance.playerID) {
        //    networkPlayerStats.currentHealth = e.CurrentHealth;
        //    networkPlayerStats.currentStamina = e.CurrentStamina;
        //    healthBar.fillAmount = Mathf.Clamp(((networkPlayerStats.currentHealth / networkPlayerStats.maxHealth)), 0, networkPlayerStats.maxHealth);
        //    staminaBar.fillAmount = Mathf.Clamp(((networkPlayerStats.currentStamina / networkPlayerStats.maxStamina)), 0, networkPlayerStats.maxStamina);
        //}
    }

    /// <summary>
    /// This is only called locally by the owner, never on a network owned UI
    /// </summary>
    public void SetInfo() {
        nameLabel.text = PlayerStats.Instance.ColoredName + " (" + PlayerStats.Instance.playerID + ")";
        SetID(PlayerStats.Instance.playerID);
        UpdateUI();
    }

    public void UpdateUI() {
        if (ownerID == PlayerStats.Instance.playerID) {
            healthBar.fillAmount = Mathf.Clamp(((PlayerStats.Instance.currentHealth / PlayerStats.Instance.maxHealth)), 0, PlayerStats.Instance.maxHealth);
            staminaBar.fillAmount = Mathf.Clamp(((PlayerStats.Instance.currentStamina / PlayerStats.Instance.maxStamina)), 0, PlayerStats.Instance.maxStamina);
        }
        //else if (networkPlayerStats != null) {
        //    healthBar.fillAmount = Mathf.Clamp(((networkPlayerStats.currentHealth / networkPlayerStats.maxHealth)), 0, networkPlayerStats.maxHealth);
        //    staminaBar.fillAmount = Mathf.Clamp(((networkPlayerStats.currentStamina / networkPlayerStats.maxStamina)), 0, networkPlayerStats.maxStamina);
        //}
        //else {
        //    Debug.LogError("networkPlayerStats was NULL in UpdateUI()");
        //}
    }

	// Update is called once per frame
	void Update () {
        if (isActive) {
            RefillStamina();
        }
	}
}
