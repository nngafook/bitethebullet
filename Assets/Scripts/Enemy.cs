﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using Pathfinding;

public class Enemy : Bolt.EntityEventListener<IEnemyState> {

    protected Player targetPlayer;

    private AIPathController aiPathController;
    private Seeker seeker;
    private Vector3 playerPosition;

    void Awake() {
        seeker = GetComponent<Seeker>();
        aiPathController = GetComponent<AIPathController>();
    }

    public override void Attached() {
        base.Attached();
        
        Debug.Log("Attached");

        state.SetTransforms(state.Transform, transform);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        targetPlayer = (players.RandomElement<GameObject>()).GetComponent<Player>();
    }

    public void Start() {
        StartCoroutine(TargetPlayer());
    }
    public void OnPathComplete(Path p) {
        Debug.Log("Yay, we got a path back. Did it have an error? " + p.error);
    }

    void OnDisable() {
        seeker.pathCallback -= OnPathComplete;
    } 

    private IEnumerator TargetPlayer() {
        while (true) {
            if (targetPlayer != null && playerPosition != targetPlayer.transform.position) {
                playerPosition = targetPlayer.transform.position;
                //navAgent.SetDestination(playerPosition);
                //seeker.StartPath(transform.position, playerPosition, OnPathComplete);
                aiPathController.targetPosition = targetPlayer.transform;
            }
            yield return new WaitForSeconds(1);
        }
    }
	
	// Update is called once per frame
	void Update () {
	    
	}
}
