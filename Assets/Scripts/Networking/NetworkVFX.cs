﻿using UnityEngine;
using System.Collections;
using Bolt;
using PathologicalGames;

public class NetworkVFX : EntityBehaviour<IVFXState> {

    private bool networkAttached = false;

    void OnEnable() {
        StartCoroutine("CheckIfAlive");
    }

    public override void Attached() {
        base.Attached();

        networkAttached = true;
    }

    public override void Detached() {
        base.Detached();
        networkAttached = false;
    }

    IEnumerator CheckIfAlive() {
        while (true) {
            yield return new WaitForSeconds(0.5f);
            if (!GetComponent<ParticleSystem>().IsAlive(true)) {
                if (networkAttached && BoltNetwork.isConnected) {
                    BoltNetwork.Destroy(this.gameObject);
                }
                else {
                    //GameObject.Destroy(this.gameObject);
                    PoolManager.Pools["VFX"].Despawn(gameObject.transform);
                }
                break;
            }
        }
    }
}
