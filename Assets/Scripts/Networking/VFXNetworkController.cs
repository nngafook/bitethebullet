﻿using UnityEngine;
using System.Collections;

public class VFXNetworkController : Bolt.EntityBehaviour<IVFXState> {

    #region NETWORKING
    public override void Attached() {
        Debug.Log("VFX Controller Attached");
        state.SetTransforms(state.Transform, transform);
    }
    #endregion
}
