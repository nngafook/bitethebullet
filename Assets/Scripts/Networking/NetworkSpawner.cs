﻿using UnityEngine;
using System.Collections;
using PathologicalGames;

public class NetworkSpawner : MonoBehaviour {

    private static NetworkSpawner instance = null;
    public static NetworkSpawner Instance { get { return instance; } }

    private SpawnPool projectilePool;
    private SpawnPool vfxPool;
    private SpawnPool entityPool;

    void Awake() {
        instance = this;
    }

    void Start() {
        projectilePool = PoolManager.Pools["Projectiles"];
        vfxPool = PoolManager.Pools["VFX"];
        entityPool = PoolManager.Pools["Entities"];
    }

    public void SpawnFromPool(SpawnFromPoolEvent e) {
        SpawnPool pool = PoolManager.Pools[e.PoolName];
        GameObject go = pool.Spawn(e.PrefabName, e.Position, e.Rotation).gameObject;

        if (e.PrefabName == "Bullet") {
            go.GetComponent<Projectile>().Fire(e.Speed);
        }
    }

    public void SpawnProjectile(ProjectileEvent e) {
        Projectile bullet = projectilePool.Spawn(e.PrefabName, e.Position, e.Rotation).gameObject.GetComponent<Projectile>();
        bullet.Fire(e.Speed);
    }
    
    public void SpawnVFX(VFXEvent e) {
        vfxPool.Spawn(e.PrefabName, e.Position, e.Rotation);
    }

    public void SpawnEntity(EntityEvent e) {
        //entityPool.Spawn(e.PrefabName, e.Position, Quaternion.identity);
        BoltNetwork.Instantiate(e.BoltPrefabID, e.Position, e.Rotation);
    }

}
