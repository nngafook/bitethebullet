﻿using UnityEngine;
using System.Collections;
using UdpKit;
using Bolt;

public class FholmNetwork : Bolt.GlobalEventListener {
    bool firstUpdate = false;
    int count = 0;
    Ping myPing;
    string[] pingList = new string[20];
    int[] pingResult = new int[20];
    int listCount = 0;
    enum State {
        SelectPeer,
        ServerBrowser,
        Empty
    }
    State state = State.SelectPeer;

    public void Awake() {
        BoltLauncher.StartClient();
    }


    public override void ZeusConnected(UdpEndPoint endpoint) {
        Debug.Log("FholmNetwork Zeus Connected");
        Bolt.Zeus.RequestSessionList();

    }

    public void Update() {

        if (listCount < (pingList.Length)) {

            //Debug.Log(listCount);

            if (myPing != null) {
                pingResult[listCount] = myPing.time;

                if (listCount < (pingList.Length - 1))
                    listCount++;
            }

            //Debug.Log(pingList[0]);

            if (pingList[listCount] != null)
                if (myPing != null || listCount == 0)
                    myPing = new Ping(pingList[listCount]);
        }
    }

    void SelectPeer() {
        if (GUILayout.Button("Start Server", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true))) {
            if (BoltNetwork.isClient == false) {
                state = State.Empty;
                BoltLauncher.StartServer(UdpKit.UdpEndPoint.Parse("0.0.0.0:27000"));
            }
        }
        if (GUILayout.Button("Start Client", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true))) {
            BoltLauncher.StartClient();
            state = State.ServerBrowser;
        }
        GUILayout.EndArea();
        GUILayout.BeginArea(new Rect(10, (Screen.height / 2) + 10, Screen.width / 2, (Screen.height / 2) - 20), GUI.skin.box);
        GUILayout.Label(string.Format("Sessions: {0}", BoltNetwork.SessionList.Count));
        foreach (var kvp in BoltNetwork.SessionList) {
            GUILayout.Label(kvp.Value.HostName);
        }
    }
    void ServerBrowser() {
        GUILayout.Label("Server Browser");
        GUILayout.BeginVertical(GUI.skin.box);
        int pingCount = 0;
        foreach (var session in BoltNetwork.SessionList) {

            if (session.Value.HostName.IndexOf("- TS") == 0){
                GUILayout.BeginHorizontal();
                GUILayout.Label(session.Value.HostName + " " + (session.Value.ConnectionsCurrent + 1) + "/" + (session.Value.ConnectionsMax + 1) + " Ping: " + pingResult[pingCount]);

                //Debug.Log(session.Value.LanEndPoint);
                //Debug.Log(session.Value.WanEndPoint);
                pingList[pingCount] = session.Value.WanEndPoint.Address.ToString();

                pingCount++;


                if (GUILayout.Button("Join")) {
                    if (BoltNetwork.isClient) {
                        Debug.Log(session.Value);
                        BoltNetwork.Connect(session.Value);
                        state = State.Empty;
                    }

                }
            GUILayout.EndHorizontal();
            }
        }
        GUILayout.EndVertical();
    }
    void OnGUI() {
        GUILayout.BeginArea(new Rect(10, 10, 400, (Screen.height / 2) - 20));
        switch (state) {
            case State.SelectPeer: SelectPeer(); break;
            case State.ServerBrowser: ServerBrowser(); break;
        }
        GUILayout.EndArea();
    }

    public void ShutdownBolt() {
        if (firstUpdate == false) {
            Debug.Log("Shutdown bolt");
            BoltLauncher.Shutdown();
            firstUpdate = true;
        }
    }

    public override void SessionListUpdated(Map<System.Guid, UdpSession> sessionList) {
        BoltLog.Info("Session list updated.");
        Debug.Log("Session list updated.");
        Invoke("ShutdownBolt", 1.0f);

    }

    public override void SessionConnectFailed(UdpSession session, IProtocolToken token) {
        BoltLog.Info("Failed to connect to session.");
        state = State.SelectPeer;
    }

    public override void ZeusConnectFailed(UdpEndPoint endpoint) {
        BoltLog.Info("Failed to connect to Zeus.");
    }

    public override void ZeusDisconnected(UdpEndPoint endpoint) {
        BoltLog.Info("Disconnected from Zeus.");
    }
    public override void ZeusNatProbeResult(NatFeatures features) {
        BoltLog.Info("Receieved results of Zeus NAT Probe.");
    }


    public override void BoltStartDone() {
        Debug.Log("BOLT START DONE");
        if (BoltNetwork.isServer) {
            Debug.Log("AAA");
            BoltNetwork.SetHostInfo("- TS Test Server -", null);
            //BoltNetwork.SetDedicatedServerInfo("TestServer", null);
            BoltNetwork.LoadScene("TestPlace");
        }
    }
}

    // HOST
        // LOADING SCREEN
            // CONNECT, CHECK FOR SERVER
                // TRUE
                    // DISCONNECT
                        // RETURN TO NETWORK MENU
                // FALSE
                    // LAUNCH AS SERVER
    // JOIN
        // LOADING SCREEN
            // CONNECT
                // CHECK IF SERVER EXISTS (THIS MIGHT HAPPEN AUTOMATICALLY)
                    // TRUE
                        // JOIN
                    // FALSE
                        // CHECK ZEUS FOR SESSIONS
                            // TRUE
                                // JOIN
                            // FALSE
                                // DISCONNECT
                                    // RETURN TO NETWORK MENU