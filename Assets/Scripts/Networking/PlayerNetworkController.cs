﻿using UnityEngine;
using System.Collections;

public class PlayerNetworkController : Bolt.EntityBehaviour<IPlayerState> {

    public bool IsOwner { get { return entity.isOwner; } }

    public override void Attached() {
        state.SetTransforms(state.Transform, transform);
    }
	
}
