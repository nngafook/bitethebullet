﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UdpKit;
using Bolt;
using PathologicalGames;

[BoltGlobalBehaviour]
public class NetworkCallbacks : GlobalEventListener {

    public override void BoltStartDone() {
        base.BoltStartDone();

        BoltNetwork.SetPrefabPool(new VFXNetworkPrefabPool());
        if (BoltNetwork.IsSinglePlayer) {
            Vector3 pos = new Vector3(0, 1.5f, -65);
            BoltNetwork.Instantiate(BoltPrefabs.Player, pos, Quaternion.identity);
        }
    }

    public override void ZeusConnected(UdpEndPoint endpoint) {
        Debug.Log("NetworkCallbacks Zeus Connected");
    }

    public override void SceneLoadLocalDone(string map) {
        base.SceneLoadLocalDone(map);

        if (map == "TestPlace") {
            Vector3 pos = new Vector3(0, 1.5f, -65);
            BoltNetwork.Instantiate(BoltPrefabs.Player, pos, Quaternion.identity);
            NetworkManager.Instance.DispatchConnectionEvent(PlayerStats.Instance.ColoredName, PlayerStats.Instance.color);
        }
    }
    
}

[BoltGlobalBehaviourAttribute(BoltNetworkModes.Host)]
public class HostNetworkCallbacks : Bolt.GlobalEventListener {

    public override void Connected(BoltConnection connection) {
        base.Connected(connection);
        
        // Not reliable for clients. Happens before Attached()

        UIController.Instance.AddChatMessage("Someone connected with a id of: " + connection.ConnectionId);
    }

}

[BoltGlobalBehaviour(BoltNetworkModes.Client)]
public class ClientNetworkCallbacks : Bolt.GlobalEventListener {

    public override void Connected(BoltConnection connection) {
        base.Connected(connection);
        Debug.Log("------- (Client) Connected Callback ------ ");
        //PlayerStats.Instance.playerID = (int)connection.ConnectionId;

        // Not reliable for clients. Happens before Attached()

        //UIController.Instance.AddChatMessage(PlayerStats.Instance.name + " ConnectionId: " + connection.ConnectionId.ToString());
        //NetworkManager.Instance.DispatchChatMessage(PlayerStats.Instance.ColoredName, PlayerStats.Instance.color, "I HAVE ARRIVED!");
    }

}

public class VFXNetworkPrefabPool : Bolt.IPrefabPool {

    GameObject IPrefabPool.Instantiate(PrefabId prefabId, Vector3 position, Quaternion rotation) {
        GameObject go = null;

        //if (prefabId == BoltPrefabs.Bullet) {
        //    SpawnPool bulletPool = PoolManager.Pools["Projectiles"];
        //    go = bulletPool.Spawn("Bullet", position, rotation).gameObject;  
        //}
        //if (prefabId == BoltPrefabs.DashSmokePuff) {
        //    SpawnPool vfxPool = PoolManager.Pools["VFX"];
        //    go = vfxPool.Spawn("DashSmokePuff", position, rotation).gameObject;  
        //}
        //else if (prefabId == BoltPrefabs.SmallExplosion) {
        //    SpawnPool vfxPool = PoolManager.Pools["VFX"];
        //    go = vfxPool.Spawn("SmallExplosion", position, rotation).gameObject;
        //}
        if (prefabId == BoltPrefabs.Enemy) {
            SpawnPool entityPool = PoolManager.Pools["Entities"];
            go = entityPool.Spawn("Enemy", position, rotation).gameObject;
        }
        else {
            go = (GameObject)GameObject.Instantiate(((IPrefabPool)this).LoadPrefab(prefabId), position, rotation);
            go.GetComponent<BoltEntity>().enabled = true;
        }

        return go;
    }

    GameObject IPrefabPool.LoadPrefab(PrefabId prefabId) {
        return PrefabDatabase.Find(prefabId);
    }

    void IPrefabPool.Destroy(GameObject gameObject) {
        PrefabId prefabId = gameObject.GetComponent<BoltEntity>().prefabId;

        //if (prefabId == BoltPrefabs.Bullet) {
        //    PoolManager.Pools["Projectiles"].Despawn(gameObject.transform);
        //}
        //if (prefabId == BoltPrefabs.DashSmokePuff || prefabId == BoltPrefabs.SmallExplosion) {
        //    PoolManager.Pools["VFX"].Despawn(gameObject.transform);
        //}
        if (prefabId == BoltPrefabs.Enemy) {
            PoolManager.Pools["Entities"].Despawn(gameObject.transform);
        }
        else {
            GameObject.Destroy(gameObject);
        }
    }

}