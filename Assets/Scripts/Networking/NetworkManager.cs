﻿using UnityEngine;
using System.Collections;
using UdpKit;
using UnityEngine.SceneManagement;
using PathologicalGames;
using System.Collections.Generic;

public class NetworkManager : Bolt.GlobalEventListener {


    #region SINGLETON
    private static NetworkManager instance = null;
    private static bool isShuttingDown;
    private static bool hasBeenAwaked = false;
    #endregion SINGLETON

    public static NetworkManager Instance {
        get {
            // If there is no _Instance for DialogManager yet and we're not shutting down at the moment
            if (instance == null && !isShuttingDown) {
                //Try finding and _Instance in the scene
                instance = GameObject.FindObjectOfType<NetworkManager>();
                //If no _Instance was found, let's create one
                if (!instance) {
                    GameObject singleton = (GameObject)Instantiate(Resources.Load("Prefabs/NetworkManager"));
                    singleton.name = "NetworkManager";
                    instance = singleton.GetComponent<NetworkManager>();
                }
                //Set the _Instance to persist between levels.
                DontDestroyOnLoad(instance.gameObject);
            }
            //Return an _Instance, either that we found or that we created.
            return instance;
        }
    }

    void OnApplicationQuit() {
        isShuttingDown = true;
    }


    private Dictionary<int, int> playerDictionary = new Dictionary<int, int>();


    void Awake() {
        //If there is no _Instance of this currently in the scene
        if (instance == null) {
            //Set ourselves as the _Instance and mark us to persist between scenes
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else {
            //If there is already an _Instance of this and It's not me, then destroy me as there should only be one.
            if (this != instance)
                Destroy(this.gameObject);
        }

        SingletonAwake();
    }

    private void SingletonAwake() {
        if (!hasBeenAwaked) {
            hasBeenAwaked = true;

            Debug.Log("SingletonAwake");
            if (SceneManager.GetActiveScene().name == "TestPlace" && !BoltNetwork.isConnected) {
                //BoltLauncher.StartServer(UdpKit.UdpEndPoint.Parse("0.0.0.0:27000"));
                BoltLauncher.StartSinglePlayer();
            }
        }
    }

	// Use this for initialization
	void Start () {
	
	}

    public void AddPlayer(int key, int value) {
        if (!playerDictionary.ContainsKey(key)) {
            playerDictionary[key] = value;
            UIController.Instance.AddChatMessage("Added NetworkID: " + key + " for Player: " + value);
        }
    }

    #region NETWORK_EVENTS
    public void SpawnFromPool(string poolName, string prefabName, Vector3 pos, Quaternion rotation, float speed = 0) {
        SpawnFromPoolEvent e = SpawnFromPoolEvent.Create();
        e.PoolName = poolName;
        e.PrefabName = prefabName;
        e.Position = pos;
        e.Rotation = rotation;
        e.Speed = speed;
        e.Send();
    }

    public void DispatchVFXEvent(string name, Vector3 pos, Quaternion rotation) {
        VFXEvent e = VFXEvent.Create();
        e.PrefabName = name;
        e.Position = pos;
        e.Rotation = rotation;
        e.Send();
    }

    public void DispatchProjectileEvent(string name, Vector3 pos, Quaternion rotation, float speed) {
        ProjectileEvent e = ProjectileEvent.Create();
        e.PrefabName = name;
        e.Position = pos;
        e.Rotation = rotation;
        e.Speed = speed;
        e.Send();
    }

    public void DispatchEntityEvent(Bolt.PrefabId prefabID, Vector3 pos, Quaternion rotation) {
        EntityEvent e = EntityEvent.Create();
        e.BoltPrefabID = prefabID;
        e.Position = pos;
        e.Rotation = rotation;
        e.Send();
    }

    public void DispatchChatMessage(string playerName, Color playerColor, string message) {
        ChatEvent e = ChatEvent.Create();
        e.Message = message;
        e.PlayerName = playerName;
        e.PlayerColor = playerColor;

        e.Send();
    }

    public void DispatchConnectionEvent(string playerName, Color playerColor) {
        ConnectionEvent e = ConnectionEvent.Create();
        e.PlayerName = playerName;
        e.PlayerColor = playerColor;

        e.Send();
    }

    /// <summary>
    /// Sent by server.
    /// Tells clients to send an event with their network and player id for everyone to sync
    /// </summary>
    public void DispatchRegisterIDEvent() {
        CommandEvent e = CommandEvent.Create();
        e.Command = "RegisterID";

        e.Send();
    }

    /// <summary>
    /// When clients receive DispatchRegisterIDEvent() this is called to send out their network and player IDs
    /// </summary>
    public void DispatchIDEvent() {
        IDEvent e = IDEvent.Create();
        e.NetworkID = PlayerStats.Instance.networkID;
        e.PlayerID = PlayerStats.Instance.playerID;

        e.Send();
    }

    /// <summary>
    /// Sent by UIController every time a client connects.
    /// This tells all clients to dispatch their name and color and every client will assign the UI associated
    /// </summary>
    public void DispatchGetPlayerInfoEvent() {
        PlayerStatsEvent e = PlayerStatsEvent.Create();
        e.Type = "GetInfo";

        e.Send();
    }

    /// <summary>
    /// This is sent when the event sent by DispatchGetPlayerInfoEvent is received
    /// </summary>
    public void DispatchPlayerStatusInfo() {
        PlayerStatsEvent e = PlayerStats.ToPlayerStatsEvent("SetInfo");
        e.Send();
    }


    #endregion NETWORK_EVENTS

    #region NETWORK_EVENT_CALLBACKS
    public override void OnEvent(SpawnFromPoolEvent e) {
        base.OnEvent(e);

        NetworkSpawner.Instance.SpawnFromPool(e);
    }

    public override void OnEvent(VFXEvent e) {
        base.OnEvent(e);
        
        NetworkSpawner.Instance.SpawnVFX(e);
    }

    public override void OnEvent(ProjectileEvent e) {
        base.OnEvent(e);

        NetworkSpawner.Instance.SpawnProjectile(e);
    }

    public override void OnEvent(EntityEvent e) {
        base.OnEvent(e);

        NetworkSpawner.Instance.SpawnEntity(e);
    }

    public override void OnEvent(ChatEvent e) {
        base.OnEvent(e);

        //UIController.Instance.AddChatMessage(PlayerStats.Instance.name + " PLAYER ID: " + PlayerStats.Instance.playerID);
        UIController.Instance.AddChatMessage(e.PlayerName, e.PlayerColor, e.Message);
    }

    public override void OnEvent(ConnectionEvent e) {
        base.OnEvent(e);

        // This seems to be getting called on every client, whenever ANY client joins the server.
        // It gets called AFTER Attached and everything else apparently.

        UIController.Instance.AddChatMessage("On Connection Event()");

        //DispatchSetPlayerUIEvent();
    }

    /// <summary>
    /// When received, clients dispatch their player and network ID for other clients to sync
    /// </summary>
    /// <param name="e"></param>
    public override void OnEvent(CommandEvent e) {
        base.OnEvent(e);
        switch (e.Command) {
            case "RegisterID":
                NetworkManager.Instance.DispatchIDEvent();
                break;
        }
    }

    /// <summary>
    /// When received, clients use the id's pass in the event to sync network ids to player ids
    /// </summary>
    /// <param name="e"></param>
    public override void OnEvent(IDEvent e) {
        base.OnEvent(e);

        NetworkManager.Instance.AddPlayer(e.NetworkID, e.PlayerID);
    }

    public override void OnEvent(PlayerStatsEvent e) {
        base.OnEvent(e);
        switch (e.Type) {
            case "GetInfo":
                // When clients receive this type, they dispatch their name and color to the other clients
                DispatchPlayerStatusInfo();
                break;
            case "SetInfo":
                // When clients receive this type, they dispatch the events to the UIController for it to set the appropriate UIs.
                UIController.Instance.SetPlayerUI(e);
                break;
        }
    }
    #endregion NETWORK_EVENT_CALLBACKS

    // Update is called once per frame
	void Update () {
	
	}
}