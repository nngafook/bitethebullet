﻿using UnityEngine;
using System.Collections;

public class EntitySpawnPoint : MonoBehaviour {

    public int maxSpawn = 5;

	// Use this for initialization
	void Start () {
        StartCoroutine(StartSpawnTimer());
	}

    private IEnumerator StartSpawnTimer() {
        int spawned = 0;
        while (true && spawned < maxSpawn) {
            yield return new WaitForSeconds(5);
            Vector3 pos = transform.position;
            pos.x += Random.Range(-10, 10);
            pos.z += Random.Range(-10, 10);
            //NetworkManager.Instance.DispatchEntityEvent(BoltPrefabs.Enemy, pos, Quaternion.identity);
            BoltNetwork.Instantiate(BoltPrefabs.Enemy, pos, Quaternion.identity);
            spawned++;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
