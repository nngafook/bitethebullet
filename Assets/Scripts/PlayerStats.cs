﻿using UnityEngine;
using System.Collections;

public class PlayerStats  {

    private static PlayerStats instance = null;
    public static PlayerStats Instance {
        get {
            if (instance == null) {
                instance = new PlayerStats();
            }
            return instance;
        }
    }

    public Color color = Utility.RandomColor();

    public string name = Utility.RandomName();
    public string ColoredName { get { return name.Colored(color); } }

    public int networkID = -1;
    public int playerID = -1;
    public float currentHealth = 3;
    public float currentStamina = 100;
    public float maxHealth = 3;
    public float maxStamina = 100;

    public float staminaRechargeRate = 0.25f;

    public static PlayerStatsEvent ToPlayerStatsEvent(string type) {
        PlayerStatsEvent e = PlayerStatsEvent.Create();

        e.Type = type;
        e.ID = instance.playerID;
        e.Color = instance.color;
        e.Name = instance.name;
        e.CurrentHealth = instance.currentHealth;
        e.MaxHealth = instance.maxHealth;
        e.CurrentStamina = instance.currentStamina;
        e.MaxStamina = instance.maxStamina;
        e.StaminaRechargeRate = instance.staminaRechargeRate;
        e.NetworkID = instance.networkID;

        return e;
    }

}