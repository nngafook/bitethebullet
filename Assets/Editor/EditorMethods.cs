﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class EditorMethods {

    #region EDITOR_MENU_METHODS
    [MenuItem("Assets/Copy Path %#c")]
    public static void CopyPath() {
        UnityEngine.Object obj = Selection.activeObject;
        if (obj == null) {
            return;
        }
        GUIUtility.systemCopyBuffer = AssetDatabase.GetAssetPath(obj.GetInstanceID());
        Debug.Log("Path Copied".Bold().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));
    }
    #endregion EDITOR_MENU_METHODS
}

public class EditorWindowMethods : EditorWindow {
    [MenuItem("Window/Close Window %w")]
    static void CloseWindow() {
        if (focusedWindow != null) {
            //if (focusedWindow.GetType() == typeof(QuestionEditorWindow) || focusedWindow.GetType() == typeof(QuestionSetGenerationRulesWindow)) {
            //    focusedWindow.Close();
            //}
        }
    }
}