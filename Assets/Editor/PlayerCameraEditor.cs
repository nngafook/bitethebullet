﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(PlayerCamera))]
public class PlayerCameraEditor : Editor {

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        if (GUILayout.Button("Set Offset")) {
            PlayerCamera cam = (target as PlayerCamera);
            Transform player = GameObject.FindGameObjectWithTag("Player").transform;
            if (player != null) {
                cam.offset = cam.transform.position - player.position;
            }
        }
    }
	
}
